// Retrieving data and returning titles
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data)
	let titles = data.map(function(sample){
		return sample.title
	})
	console.log(titles)
})

// Retrieving a single to do list item and printing its title and status
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => {
	console.log(data)
	console.log('The item "' + data.title + '" on the list has a status of ' + data.completed)
})

// Creating a to do list item
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		title: 'Created To Do List Item',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// Updating a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		title: 'Updated To Do List Item',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "10/03/22",
		status: "Complete",
		title: 'delectus aut autem',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// Deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})